package telegram

type UpdateResponse struct {
	Ok     bool      `json:"ok"`
	Result *[]Update `json:"result"`
}
