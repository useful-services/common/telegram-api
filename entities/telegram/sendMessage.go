package telegram

type SendMessage struct {
	ChatId                   *int64           `json:"chat_id,omitempty"`                     // Unique identifier for the target chat or username of the target channel (in the format @channelusername)
	MessageThreadId          *int64           `json:"message_thread_id,omitempty"`           // Optional. Unique identifier for the target message thread (topic) of the forum; for forum supergroups only
	Text                     *string          `json:"text,omitempty"`                        // Text of the message to be sent, 1-4096 characters after entities parsing
	ParseMode                *string          `json:"parse_mode,omitempty"`                  // Optional. Mode for parsing entities in the message text. See formatting options for more details.
	Entities                 []*MessageEntity `json:"entities,omitempty"`                    // Optional. A JSON-serialized list of special Entities that appear in message text, which can be specified instead of parse_mode
	DisableWebPagePreview    *bool            `json:"disable_web_page_preview,omitempty"`    // Optional. Disables link previews for links in this message
	DisableNotification      *bool            `json:"disable_notification,omitempty"`        // Optional. Sends the message silently. Users will receive a notification with no sound.
	ProtectContent           *bool            `json:"protect_content,omitempty"`             // Optional. Protects the contents of the sent message from forwarding and saving
	ReplyToMessageId         *int64           `json:"reply_to_message_id,omitempty"`         // Optional. If the message is a reply, ID of the original message
	AllowSendingWithoutReply *bool            `json:"allow_sending_without_reply,omitempty"` // Optional. Pass True if the message should be sent even if the specified replied-to message is not found
	//reply_markup InlineKeyboardMarkup or ReplyKeyboardMarkup or ReplyKeyboardRemove or ForceReply // Optional. Additional interface options. A JSON-serialized object for an inline keyboard, custom reply keyboard, instructions to remove reply keyboard or to force a reply from the user.
}
