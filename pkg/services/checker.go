package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/useful-services/common/telegram-api/entities/telegram"
	"io"
	"net/http"
	"sync/atomic"
)

const (
	telegramApiUrl = "https://api.telegram.org/bot%s"
)

type Checker struct {
	offset *int64
}

func NewChecker() *Checker {
	offset := int64(0)
	return &Checker{offset: &offset}
}

func (c *Checker) SetWebhook(token string, url string, ipAddress *string, maxConnections *int64) (*http.Response, error) {
	req := telegram.Webhook{Url: url, IpAddress: ipAddress, MaxConnections: maxConnections}

	body, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	return http.Post(
		fmt.Sprintf(
			telegramApiUrl+"/setWebhook",
			token,
		),
		"application/json",
		bytes.NewBuffer(body),
	)
}

func (c *Checker) DeleteWebhook(token string) (*http.Response, error) {
	return c.SetWebhook(token, "", nil, nil)
}

func (c *Checker) GetUpdates(token string) (*[]telegram.Update, error) {
	resp, err := c.GetUpdatesSilently(token)
	if err != nil {
		return resp, err
	}

	if len(*resp) > 0 {
		atomic.StoreInt64(c.offset, *(*resp)[len(*resp)-1].UpdateId+1)
	}
	return resp, err
}

func (c *Checker) GetUpdatesSilently(token string) (*[]telegram.Update, error) {
	buf := atomic.LoadInt64(c.offset)
	req := telegram.GetUpdates{Offset: &buf}

	body, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(
		fmt.Sprintf(
			telegramApiUrl+"/getUpdates",
			token,
		),
		"application/json",
		bytes.NewBuffer(body),
	)
	if err != nil {
		return nil, err
	}

	defer func(Body io.ReadCloser) {
		err = Body.Close()
		if err != nil {
			return
		}
	}(resp.Body)

	result := &telegram.UpdateResponse{}
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respBody, result)
	return result.Result, err
}
