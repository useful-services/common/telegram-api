package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/useful-services/common/telegram-api/entities/telegram"
	"gitlab.com/useful-services/common/telegram-api/pkg/errors_wrapper"
	"net/http"
)

type Sender struct {
}

func (s *Sender) SendMessage(msg *telegram.SendMessage, token string) error {
	if msg.ChatId == nil {
		return errors_wrapper.NilPointerChatId
	}

	body, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	resp, err := http.Post(
		fmt.Sprintf(
			telegramApiUrl+"/sendMessage",
			token,
		),
		"application/json",
		bytes.NewBuffer(body),
	)
	if err != nil {
		return err
	}
	if resp.StatusCode != 200 {
		return fmt.Errorf(errors_wrapper.CodeNot200.Error(), resp.StatusCode)
	}

	return err
}
