package errors_wrapper

import "errors"

var (
	NilPointerChatId = errors.New("chat id required")
	CodeNot200       = errors.New("%s: %s")
)
